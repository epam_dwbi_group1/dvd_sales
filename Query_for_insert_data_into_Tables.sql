INSERT INTO film ([name], [year], duration, budget, [description], rating)
VALUES  ('filmName',getdate(),'1:11:11',100000,'filmDescription',11)

INSERT INTO genre ([name])
VALUES  ('genreName')

INSERT INTO category ([name])
VALUES  ('categoryName')

INSERT INTO award ([name])
VALUES  ('awardName')

INSERT INTO film_category (category_id, film_id)
VALUES  (1,1)

INSERT INTO film_genre (genre_id, film_id)
VALUES  (1,1)

INSERT INTO film_award (film_id, award_id)
VALUES  (1,1)

INSERT INTO actor ([name], surname, gender, birthdate, award_id)
VALUES  ('actorName','actorSurname','m',CONVERT(date, getdate()),1)

INSERT INTO actor_award (actor_id, award_id)
VALUES  (1,1)

INSERT INTO position ([name])
VALUES  ('positionName')

INSERT INTO superior ([name])
VALUES  ('superiorName')

INSERT INTO position_superior (position_id, superior_id)
VALUES  (1,1)

INSERT INTO country ([name])
VALUES  ('countryName')

INSERT INTO [address] (country_id, city, street, building, appartment, zip_code)
VALUES  (1,'addressCity','addressStreet','adrBuild',11,'010101')

INSERT INTO employee (name, surname, position_id, employment_date, salary, bonus_to_salary, email, address_id)
VALUES  ('employeeName','employeeSurame',1,getdate(),1000,1,'employeeEmail', 1)

INSERT INTO manager ([name])
VALUES  ('managerName')

INSERT INTO shop (shop_name, address_id, http_adress, email, manager_id, phone_number, opening_hours)
VALUES  ('shopName',1,'shopHttp','shopEmail',1,'000-000-0000','09-18')

INSERT INTO payment_type ([name])
VALUES  ('paymentName')

INSERT INTO payment (payment_type_id, amount)
VALUES  (1,100)

INSERT INTO operation_type ([name])
VALUES  ('opName')

INSERT INTO operation ([date], return_date, [type_id], payment_id)
VALUES  (getdate(),getdate(),1,1)

INSERT INTO delivery_type ([name])
VALUES  ('dNam')

INSERT INTO delivery (delivery_date, comment, delivery_type_id)
VALUES  (getdate(),'deliveryComment',1)

INSERT INTO discount_card (id, discount_amount, [start_date])
VALUES  ('8024E249-A690-4303-805B-EB75CADD3D0F',11,getdate())

INSERT INTO client ([name], surname, phone_number, address_id, passport_id, email, skype, discount_card_no)
VALUES  ('clientName','clientSurname','000-000-0000',1,'53C18923-F487-48C8-85A4-5F3D04169B67','clientEmail','clientSkype','8024E249-A690-4303-805B-EB75CADD3D0F')

INSERT INTO [order] (client_id, delivery_id)
VALUES  (1,1)

INSERT INTO order_delivery_type (order_id, delivery_id)
VALUES  (1,1)

INSERT INTO order_address (order_id, address_id)
VALUES  (1,1)

INSERT INTO publisher ([name])
VALUES  ('publisherName')

INSERT INTO [format] ([name])
VALUES  ('formatName')

INSERT INTO [disk] (id, format_id, [3d_option], publisher_id, sale_price, daily_rent_price)
VALUES  ('A55C117D-CBFD-479A-A9AC-C9B803F1E4A6',1,1,1,100,5)

INSERT INTO order_disk (order_id, disk_id)
VALUES  (1,'A55C117D-CBFD-479A-A9AC-C9B803F1E4A6')

INSERT INTO disk_film (disk_id, film_id)
VALUES  ('A55C117D-CBFD-479A-A9AC-C9B803F1E4A6',1)

INSERT INTO [language] ([name])
VALUES  ('languageName')

INSERT INTO production ([name])
VALUES  ('productionName')

INSERT INTO director ([name], surname, film_id)
VALUES  ('directorName','directorSurname',1)

INSERT INTO film_subtitles (film_id, lang_id)
VALUES  (1,1)

INSERT INTO film_audio (film_id, lang_id)
VALUES  (1,1)

INSERT INTO film_production (production_id, film_id)
VALUES  (1,1)

INSERT INTO film_actor (actor_id, film_id, [name], surname, birth_date)
VALUES  (1,1,'actorName','actorSurname',getdate())

INSERT INTO film_country (country_id, film_id)
VALUES  (1,1)

INSERT INTO film_director (director_id, film_id)
VALUES  (1,1)

SELECT * FROM film
SELECT * FROM genre
SELECT * FROM category
SELECT * FROM award
SELECT * FROM film_category
SELECT * FROM film_genre
SELECT * FROM film_award
SELECT * FROM actor
SELECT * FROM actor_award
SELECT * FROM position
SELECT * FROM superior
SELECT * FROM position_superior
SELECT * FROM country
SELECT * FROM [address]
SELECT * FROM employee
SELECT * FROM manager
SELECT * FROM shop
SELECT * FROM payment_type
SELECT * FROM payment
SELECT * FROM operation_type
SELECT * FROM operation
SELECT * FROM delivery_type
SELECT * FROM delivery
SELECT * FROM discount_card
SELECT * FROM client
SELECT * FROM [order]
SELECT * FROM order_delivery_type
SELECT * FROM order_address
SELECT * FROM publisher
SELECT * FROM [format]
SELECT * FROM [disk]
SELECT * FROM order_disk
SELECT * FROM disk_film
SELECT * FROM [language]
SELECT * FROM production
SELECT * FROM director
SELECT * FROM film_subtitles
SELECT * FROM film_audio
SELECT * FROM film_production
SELECT * FROM film_actor
SELECT * FROM film_country
SELECT * FROM film_director